### Kafka Sample Login

Generate kafka topic when user registered
Send message to email that are getting from the kafka topic.

### How to run?

* generate build 

```
mvn clean package -f kafka-login/pom.xml
```

* run docker-compose

```
docker-compose up --build
```

* open register.html

```
http://localhost:8080/register.html
```


