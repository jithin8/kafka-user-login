package com.jitihn.login.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.mailer.ReactiveMailer;

@Path("/hello")
public class MailerEx {

    Logger LOG = LoggerFactory.getLogger(MailerEx.class);
    @Inject
    Mailer mailer;

    @Inject
    ReactiveMailer reactiveMailer;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @Incoming("user-get-stream")
    public void consume(String message) {
        String[] msg = message.split(",");
        String username = msg[0];
        String email = msg[1];
        LOG.info("get data from kafka stram with username : " + username + " email : " + email);

        LOG.info("sending mail ....");

        reactiveMailer.send(Mail.withText(email, "A reactive email from quarkus", "This is my body"))
                .thenApply(x -> Response.accepted().build());
        LOG.info("Mail send successfully!!!");
    }

    @GET
    @Path("/mail")
    @Produces(MediaType.TEXT_PLAIN)
    public String mail() {
        Mail mail = Mail.withText("benny.aitrich@gmail.com", "subject is testing",
                "This is to test if you will check the mail");
        mailer.send(mail);
        return " Mail send !";
    }
}