package com.jitihn.login.resource;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.jitihn.login.model.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.panache.common.Sort;
import io.smallrye.reactive.messaging.annotations.Emitter;
import io.smallrye.reactive.messaging.annotations.Stream;

@ApplicationScoped
@Path("/register")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RegisterResource {

    Logger LOG = LoggerFactory.getLogger(RegisterResource.class);

    @Inject
    @Stream("user-created-stream")
    Emitter<String> emitter;

    @GET
    public List<Users> get() {
        return Users.listAll(Sort.by("username"));
    }

    @POST
    @Transactional
    public Response add(Users user) {
        if (user.id != null) {
            throw new WebApplicationException("Id was invalidaly set on request", 422);
        }
        Users sameUser = Users.find("username", user.getUsername()).firstResult();
        LOG.info("Same user exists " + sameUser);
        if (sameUser != null) {
            throw new WebApplicationException("User already exists", Status.CONFLICT);
        }
        user.persist();
        LOG.info("User created successfully ", user);

        // Send object of user to Kafka topic
        StringBuilder sb = new StringBuilder();
        sb.append(user.getUsername());
        sb.append(",");
        sb.append(user.getEmail());
        emitter.send(sb.toString());
        LOG.info("User send to topic successfully");
        LOG.info(emitter.toString());

        return Response.ok(user).status(Status.CREATED).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            return Response.status(code)
                    .entity(Json.createObjectBuilder().add("error", exception.getMessage()).add("code", code).build())
                    .build();
        }

    }
}